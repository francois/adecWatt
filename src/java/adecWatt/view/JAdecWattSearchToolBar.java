package adecWatt.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import misc.Util;

@SuppressWarnings ("serial") public class JAdecWattSearchToolBar extends JToolBar implements ActionListener {

    JTextField searchTF = new JTextField (8);

    public JAdecWattSearchToolBar (final JAdecWatt jAdecWatt) {
	super ("Search");
	searchTF.getDocument ().addDocumentListener (new DocumentListener () {
		public void changedUpdate (DocumentEvent e) {
		    String search = Util.removeAccent (searchTF.getText ()).toLowerCase ();
		    jAdecWatt.searchUnit (search);
		    jAdecWatt.searchComp (search);
		}
		public void insertUpdate (DocumentEvent e) { changedUpdate (e); }
		public void removeUpdate (DocumentEvent e) { changedUpdate (e); }
	    });
	// JPanel jPanel = new JPanel (new BorderLayout ());
	// jPanel.add (Util.newIconButton ("Find", this), BorderLayout.WEST);
	// jPanel.add (searchTF, BorderLayout.CENTER);
	// Util.unBoxButton (jPanel);
	// add (jPanel, BorderLayout.NORTH);

	add (Util.newIconButton ("Search", this));
	add (searchTF);
	//Util.unBoxButton (jPanel);
    }

    public void setOrientation (int o) {
	super.setOrientation (HORIZONTAL);
    }
    public void  actionPerformed (ActionEvent e) {
	// XXX
    }
}
