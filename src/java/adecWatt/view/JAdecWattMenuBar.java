package adecWatt.view;

import java.util.Hashtable;
import javax.swing.AbstractButton;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JMenu;
import javax.swing.JMenuBar;

import misc.ApplicationManager;
import misc.ToolBarManager;
import misc.Util;

import adecWatt.control.AdecWattManager;
import adecWatt.model.User;

@SuppressWarnings ("serial") public class JAdecWattMenuBar extends JMenuBar {

    JMenu editMenu, placementMenu, yodaMenu;

    // ========================================
    public JAdecWattMenuBar (ApplicationManager controllerManager, ApplicationManager adecWattManager, ApplicationManager storyManager,
			      ApplicationManager proxyManager, ApplicationManager remoteUpdateManager, ApplicationManager helpManager,
			      ToolBarManager toolBarManager) {
	setLayout (new BoxLayout (this, BoxLayout.X_AXIS));
	JMenu fileMenu = Util.addJMenu (this, "File");
	editMenu = Util.addJMenu (this, "Edit");
	placementMenu = Util.addJMenu (this, "Placement");
	yodaMenu = Util.addJMenu (this, "Yoda");
	add (Box.createHorizontalGlue ());
	JMenu helpMenu = Util.addJMenu (this, "Help");

	storyManager.addMenuItem (editMenu);
	adecWattManager.addMenuItem (fileMenu, editMenu, placementMenu, yodaMenu, helpMenu);
	controllerManager.addMenuItem (fileMenu);
	proxyManager.addMenuItem (helpMenu);
	remoteUpdateManager.addMenuItem (helpMenu);
	helpManager.addMenuItem (helpMenu);
	toolBarManager.addMenuItem (helpMenu);

	Hashtable<String, AbstractButton> buttons = new Hashtable<String, AbstractButton> ();
	Util.collectButtons (buttons, fileMenu);
	Util.collectButtons (buttons, editMenu);
	Util.collectButtons (buttons, placementMenu);
	Util.collectButtons (buttons, helpMenu);

	controllerManager.addActiveButtons (buttons);
	adecWattManager.addActiveButtons (buttons);
	storyManager.addActiveButtons (buttons);
	proxyManager.addActiveButtons (buttons);
	remoteUpdateManager.addActiveButtons (buttons);
	helpManager.addActiveButtons (buttons);
	toolBarManager.addActiveButtons (buttons);

	Util.setAccelerator (buttons, AdecWattManager.actionsControl);
    }

    public void changeProfil (User user) {
	boolean editor = user.isEditor ();
	editMenu.setVisible (editor);
	placementMenu.setVisible (editor);
	yodaMenu.setVisible (user.isAdmin ());
    }

    // ========================================
}

