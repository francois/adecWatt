/**
 * Architecture des classes
 * 
 * JLocalizedUserLabel
 * 
 * JAdecWatt
 * JAdecWattMenuBar
 * JAdecWattToolBar
 * JAdecWattDialog
 * 
 * JUnitPopup
 * JDragUnit
 * JItemPopup
 * 
 * JWorkspaceView
 *   JBuildingView
 *   JLightplotView
 * JComp
 * 
 * JTransform : editable
 * JDefProp
 * JDefChoice
 * 
 * JEditable
 * JProp
 *   jProp...
 * 
 */
package adecWatt.view;
