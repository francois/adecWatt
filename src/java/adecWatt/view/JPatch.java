package adecWatt.view;

import java.awt.Component;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;

import misc.Bundle;
import misc.Util;
import static misc.Util.GBC;
import static misc.Util.GBCNL;

import adecWatt.model.Circuits;
import adecWatt.model.Patch;

@SuppressWarnings ("serial")
public class JPatch extends JPanel implements SwingConstants {

    static public Border groupBorder = BorderFactory.createEtchedBorder (EtchedBorder.LOWERED);

    static public class JInfo extends JPanel {
	public JInfo (Component line1, Component line2, Component line3) {
	    super (new GridLayout (3, 1));
	    add (line1);
	    add (line2);
	    add (line3);
	    setBorder (groupBorder);
	}
	public JInfo (Component line1, Component line2) {
	    super (new GridLayout (2, 1));
	    add (line1);
	    add (line2);
	    setBorder (groupBorder);
	}
	public JInfo (Component line) {
	    super (new GridLayout (1, 1));
	    add (line);
	    setBorder (groupBorder);
	}
    }

    public JPatch (Patch patch) {
	JPanel content = Util.getGridBagPanel ();
	add (Util.getJScrollPane (content));

	Util.addComponent (new JInfo (Util.newLabel ("Circuit", RIGHT),
				      Util.newLabel ("Line", RIGHT),
				      Util.newLabel ("Watt", RIGHT)), content, GBC);
	if (patch != null)
	    for (String circuitName : patch.circuitLines.keySet ()) {
		String lineNames = "", sep = "";
		for (String name : patch.circuitLines.get (circuitName)) {
		    lineNames += sep+name;
		    sep = ", ";
		}
		Util.addComponent (new JInfo (new JLabel (circuitName, CENTER),
					      new JLabel (lineNames, CENTER),
					      new JLabel (Util.toNumIn10Units (patch.circuitConsumption.get (circuitName)), CENTER)), content, GBC);
	}
	Util.addComponent (new JLabel (), content, GBCNL);
	Util.addComponent (new JInfo (Util.newLabel ("Line", RIGHT),
				      Util.newLabel ("Circuit", RIGHT),
				      Util.newLabel ("Watt", RIGHT)), content, GBC);
	if (patch != null)
	    for (String lineName : patch.getLineKeys ()) {
		String circuitName = patch.lineCircuit.get (lineName);
		JLabel consumption = null;
		if (Circuits.SHORTCUT == circuitName) {
		    circuitName = Bundle.getLabel (Circuits.SHORTCUT);
		    consumption = new JLabel ("");
		} else
		    consumption = new JLabel (Util.toNumIn10Units (patch.lineConsumption.get (lineName)), CENTER);
		Util.addComponent (new JInfo (new JLabel (lineName, CENTER),
					      new JLabel (circuitName, CENTER),
					      consumption), content, GBC);
	    }
	Util.addComponent (new JLabel (), content, GBCNL);
	Util.addComponent (new JInfo (Util.newLabel (Circuits.UNPLUG, RIGHT)), content, GBC);
	if (patch != null)
	    for (String circuitName : patch.unplug)
		Util.addComponent (new JInfo (new JLabel (circuitName, CENTER)), content, GBC);
	Util.addComponent (new JLabel (), content, GBCNL);
	Util.addComponent (new JInfo (Util.newLabel (Circuits.SHORTCUT, RIGHT)), content, GBC);
	if (patch != null)
	    for (String circuitName : patch.shortcut)
		Util.addComponent (new JInfo (new JLabel (circuitName, CENTER)), content, GBC);
	Util.addComponent (new JLabel (), content, GBCNL);
	Util.addComponent (new JInfo (Util.newLabel (Circuits.CIRCUITLESS, RIGHT)), content, GBC);
	if (patch != null)
	    for (String compName : patch.circuitless)
		Util.addComponent (new JInfo (new JLabel (compName, CENTER)), content, GBC);
	Util.addComponent (new JLabel (), content, GBCNL);
    }
}
