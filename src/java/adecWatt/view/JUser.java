package adecWatt.view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Hashtable;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import misc.Bundle;
import misc.RemoteUpdate;
import misc.RemoteUpdateManager;
import misc.Util;
import static misc.Util.GBCNL;

import adecWatt.model.User;

@SuppressWarnings ("serial")
public class JUser extends JPanel {

    // ========================================
    private User user;
    private HashSet<User.Role> roles;
    private Hashtable<User.Role, JCheckBox> rolesCB = new Hashtable<User.Role, JCheckBox> ();

    private JTextField loginTF;
    private JPasswordField passwordTF;
    private JTextField idTF;

    // ========================================
    public JUser (User user, final RemoteUpdateManager remoteUpdateManager) {
	super (new BorderLayout ());
	this.user = user;
	loginTF = new JTextField (user.getLogin ());
	passwordTF = new JPasswordField (user.getPassword ());
	idTF = new JTextField (""+user.getUserId ());
	roles = user.getRoles ();
	JPanel form = Util.getGridBagPanel ();
	Util.addLabelFields (form, "Login", loginTF);
	Util.addLabelFields (form, "Password", passwordTF);
	Util.addButton ("Connection", new ActionListener () {
		public void actionPerformed (ActionEvent e) {
		    RemoteUpdate remoteUpdate = remoteUpdateManager.getRemoteUpdate ();
		    remoteUpdate.logoutDokuwiki ();
		    char[] pass = passwordTF.getPassword ();
		    remoteUpdate.loginDokuwiki (loginTF.getText (), ""+new String (pass));
		    String newRoles = remoteUpdate.getRoles (loginTF.getText ());
		    if (newRoles == null) {
			remoteUpdateManager.setConnected (false);
			JOptionPane.showMessageDialog (JUser.this, Bundle.getMessage ("ConnectionFailed"));
			return;
		    }
		    remoteUpdateManager.setConnected (true);
		    idTF.setText (""+User.updateRoles (roles, newRoles));
		    for (User.Role role : User.Role.values ())
			rolesCB.get (role).setSelected (roles.contains (role));
		    JOptionPane.showMessageDialog (JUser.this, Bundle.getMessage ("ConnectionSucceeded"));
		}
	    }, form, GBCNL);
	int idx = 0;
	for (final User.Role role : User.Role.values ()) {
	    if (idx++ == 5) {
		Util.addComponent (new JTextArea (Bundle.getMessage ("ChangeRole")), form, GBCNL);
		Util.addLabelFields (form, "Id", idTF);
	    }
	    JCheckBox jCheckBox = Util.addCheckButton (role.toString (), new ActionListener () {
		    public void actionPerformed (ActionEvent e) {
			if (((JCheckBox) e.getSource ()).isSelected ())
			    roles.add (role);
			else
			    roles.remove (role);
		    }
		}, form, GBCNL);
	    jCheckBox.setSelected (roles.contains (role));
	    rolesCB.put (role, jCheckBox);
	}
	add (form, BorderLayout.CENTER);
    }

    // ========================================
    public void confirm () {
	user.setLogin (loginTF.getText ());
	user.setPassword (new String (passwordTF.getPassword ()));
	user.setUserId (Integer.parseInt (idTF.getText ()));
	user.setRoles (roles);
    }

    // ========================================
}
