package adecWatt.view;

//import javax.swing.UIManager;
import java.awt.Color;
import java.awt.Dimension;
import misc.ScaledImage;

import adecWatt.model.AdecWatt;
import adecWatt.model.Comp;
import adecWatt.model.Item;

@SuppressWarnings ("serial") public class JComp extends JItem {

    // ========================================
    public Comp comp;

    protected boolean isLow;
    protected ScaledImage scaledImage;
    protected Dimension tile = ScaledImage.ONE_TILE;
    //protected JLabel jLabel;

    public Item<?, ?, ?>		getItem ()		{ return comp; }
    public boolean	isLow ()		{ return isLow; }
    public ScaledImage	getScaledImage ()	{ return scaledImage; }

    // ========================================
    public JComp (JWorkspaceView<?> jWorkspaceView, Comp comp) {
	super (jWorkspaceView, comp);
    }

    // ========================================
    public void setItem (Item item) {
	if (item == null)
	    return;
	comp = (Comp) item;
	scaledImage = comp.getIcon ();
	setCurrentThetaDegree (comp.getThetaDegree ());
	updateLevel ();
	setCurrentSize ();
 	setCurrentPos ();
   }

    // ========================================
    public void setScale () {
	tile = Comp.getTile (comp.getTileSize (), comp.getSize ());
	super.setScale ();
    }

    // ========================================
    public void updateLevel () {
	if (comp.getLevel () <= AdecWatt.lowLevel == isLow)
	    return;
	isLow = ! isLow;
	if (!isLow) {
	    scaledImage = comp.getIcon ();
	    return;
	}
	ScaledImage scaledLowImage = comp.getLowIcon ();
	if (scaledLowImage != null)
	    scaledImage = scaledLowImage;
	Color compColor = comp.getColor ();
	if (compColor != null)
	    scaledImage = comp.getAdecWatt ().getIconDB ().getColoredScaledImages (scaledImage, compColor);
    }

    // ========================================
    public void updateIcon () {
	if (currentPos == null || currentSize == null)
	    return;
	tile = Comp.getTile (comp.getTileSize (), currentSize);
	updateLevel ();
	super.updateIcon ();
    }

    // ========================================
}
