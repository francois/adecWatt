package adecWatt.view;

import javax.swing.JLabel;

import adecWatt.model.Unit;

@SuppressWarnings ("serial") public class JDragUnit extends JLabel {

    // ========================================
    public Unit<?> unit;

    public JDragUnit (Unit<?> unit) {
	this.unit = unit;
	setIcon (unit.getIcon (16));
	setText (unit.toString ());
	setSize (getPreferredSize ());
    }

    // ========================================
}
