package adecWatt.view;

import java.util.Hashtable;
import javax.swing.AbstractButton;
import javax.swing.JToolBar;

import misc.ApplicationManager;
import misc.ToolBarManager;
import misc.Util;

import adecWatt.model.User;

public class JAdecWattToolBar {

    JToolBar editToolBar;
    JToolBar placementToolBar;

    // ========================================
    static public final String defaultCardinalPoint = "North";

    public JAdecWattToolBar (ApplicationManager controllerManager, ApplicationManager adecWattManager, ApplicationManager storyManager,
			      ApplicationManager proxyManager, ApplicationManager remoteUpdateManager, ApplicationManager helpManager,
			      ToolBarManager toolBarManager, JToolBar searchToolBar, JToolBar slidersToolBar) {

	JToolBar fileToolBar = toolBarManager.newJToolBar ("File", defaultCardinalPoint);
	editToolBar = toolBarManager.newJToolBar ("Edit", defaultCardinalPoint);
	placementToolBar = toolBarManager.newJToolBar ("Placement", defaultCardinalPoint);
	toolBarManager.add (searchToolBar, defaultCardinalPoint);
	toolBarManager.add (slidersToolBar, defaultCardinalPoint);
	JToolBar helpToolBar = toolBarManager.newJToolBar ("Help", defaultCardinalPoint);

	controllerManager.addIconButtons (fileToolBar);
	storyManager.addIconButtons (editToolBar);
	adecWattManager.addIconButtons (fileToolBar, editToolBar, placementToolBar, searchToolBar, slidersToolBar, helpToolBar);
	proxyManager.addIconButtons (helpToolBar);
	remoteUpdateManager.addIconButtons (helpToolBar);
	helpManager.addIconButtons (helpToolBar);
	toolBarManager.addIconButtons (helpToolBar);

	Hashtable<String, AbstractButton> buttons = new Hashtable<String, AbstractButton> ();
	Util.collectButtons (buttons, fileToolBar);
	Util.collectButtons (buttons, editToolBar);
	Util.collectButtons (buttons, placementToolBar);
	Util.collectButtons (buttons, searchToolBar);
	Util.collectButtons (buttons, slidersToolBar);
	Util.collectButtons (buttons, helpToolBar);

	controllerManager.addActiveButtons (buttons);
	adecWattManager.addActiveButtons (buttons);
	storyManager.addActiveButtons (buttons);
	proxyManager.addActiveButtons (buttons);
	remoteUpdateManager.addActiveButtons (buttons);
	helpManager.addActiveButtons (buttons);
	toolBarManager.addActiveButtons (buttons);
    }

    public void changeProfil (User user) {
	boolean editor = user.isEditor ();
	editToolBar.setVisible (editor);
	placementToolBar.setVisible (editor);
    }

    // ========================================
}
