package adecWatt.view;

import adecWatt.model.unit.Building;

@SuppressWarnings ("serial") public class JBuildingView extends JWorkspaceView<Building> {

    // ========================================
    public JBuildingView (JAdecWatt jAdecWatt, Building building) {
	super (jAdecWatt, building);
    }

    // ========================================
}
