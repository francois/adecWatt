package adecWatt.view.jProp;

import java.util.Vector;
import javax.swing.JComponent;
import javax.swing.text.JTextComponent;

import adecWatt.view.JEditable;
import static adecWatt.model.Prop.PropMix;

public class JPropEnum extends JPLabel implements JAutoComboBox.Filter {

    // ========================================
    protected JAutoComboBox choice;

    public JPropEnum (String propName, JEditable jEditable) {
	super (propName, jEditable);
    }

    @Override
    public Vector<String> match (String value) {
	value = value.toLowerCase ();
	Vector<String> founds = new Vector<String> ();
	if (enumChoice != null)
	    for (String item : enumChoice)
		if (item.toLowerCase ().indexOf (value) >= 0)
		    founds.add (item);
	return founds;
    }

    protected boolean verify (JTextComponent editor) {
	if (editor == null || enumChoice == null)
	    return false;
	boolean result = enumChoice.contains (editor.getText ());
	editor.setBorder (result ? defaultBorder : badValueBorder);
	return result;
    }

    // ========================================
    protected JComponent getEditor () {
	choice = new JAutoComboBox (this);
	JTextComponent editor = choice.getEditorComponent ();
	defaultBorder = editor.getBorder ();
	editors = new JTextComponent [] {editor};
	setChangeListener (editor);
	choice.setSelectedItem (lastValue);
	return choice;
    }

    public String getLastValue () {
	String val = (String) choice.getSelectedItem ();
	if (unchanged.size () > 0)
	    val = PropMix;
	return val;
    }

    // ========================================
}
