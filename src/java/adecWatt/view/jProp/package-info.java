/**
 * Architecture des classes
 * 
 * JFilePreview
 * JImagePopup
 * (JProp)
 *   JPLabel
 *     JPropText
 *     JPropCity
 *     JPropEnum
 *     JPropBuilding
 *     JPMultiValues
 *       JPropNumber
 *       JPropSquare
 *       JPropCube
 *   JPImage
 *     JPropIcon
 *     JPropImage
 *   JPropPreview
 *   JPropGeo
 *   JPropArticle
 * 
 */
package adecWatt.view.jProp;
