package adecWatt.view.jProp;

import java.text.NumberFormat;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

import adecWatt.view.JEditable;
import static adecWatt.model.Prop.DoubleMix;
import static adecWatt.model.Prop.PropMix;
import static adecWatt.model.Prop.numberFormat;

public abstract class JPMultiValues extends JPLabel {

    // ========================================
    protected NumberFormat getNumberFormat () { return numberFormat; }

    // ========================================
    public JPMultiValues (String propName, JEditable jEditable) {
	super (propName, jEditable);
    }

    protected boolean verify (JTextComponent editor) {
	editor.setBorder (defaultBorder);
	String txt = editor.getText ();
	if (txt.isEmpty ())
	    return true;
	try {
	    Float.parseFloat (txt.replace (",", "."));
	    return true;
	} catch (NumberFormatException nfe) {
	}
	editor.setBorder (badValueBorder);
	return false;
    }

    protected JComponent getEditor () {
	JPanel jPanel = new JPanel ();
	editors = new JTextField [nbVal];
	for (int i = 0; i < nbVal; i++) {
	    if (localizedUserMultiLabels != null && localizedUserMultiLabels.length > i)
		jPanel.add (localizedUserMultiLabels[i].getJLabel ());
	    final JTextField editor = new JTextField (lastValues == null || lastValues[i] == null ? "" :
						      getNumberFormat ().format (lastValues [i]), 6);
	    editors [i] = editor;
	    defaultBorder = editor.getBorder ();
	    boolean unchange = lastValues != null && lastValues[i] != null && lastValues[i] == DoubleMix;
	    setUnchange (editor, unchange);
	    setChangeListener (editor);
	    editors [i].setInputVerifier (new InputVerifier () {
		    @Override
		    public boolean verify (JComponent input) {
			return JPMultiValues.this.verify (editor);
		    }
		});
	    jPanel.add (editors[i]);
	}
	return jPanel;
    }

    protected JComponent getValue () {
	if (lastValues == null)
	    return null;
	JPanel jPanel = new JPanel ();
	boolean noVal = true;
	for (int i = 0; i < lastValues.length; i++) {
	    if (localizedUserMultiLabels != null && localizedUserMultiLabels.length > i)
		jPanel.add (localizedUserMultiLabels[i].getJLabel ());
	    if (lastValues[i] == null) {
		jPanel.add (getEmptyLabel ());
		continue;
	    }
	    noVal = false;
	    jPanel.add ((lastValues[i] == DoubleMix) ? getUnknownLabel () :
			new JLabel (getNumberFormat ().format (lastValues[i])));
	}
	return noVal ? null : jPanel;
    }

    public String getLastValue () {
	if (unchanged.size () >= nbVal)
	    return PropMix;
	String newVal = "";
	String sep = "";
	boolean noVal = true;
	for (int i = 0; i < nbVal; i++) {
	    newVal += sep;
	    sep = "|";
	    if (unchanged.contains (editors [i])) {
		newVal += "?";
		noVal = false;
		continue;
	    }
	    String val = editors [i].getText ().replace (",", ".");
	    if (val.isEmpty ())
		continue;
	    try {
		double dVal = Double.parseDouble (val);
		if (parentValues != null && parentValues[i] != null && parentValues[i] == dVal)
		    continue;
		newVal += getNumberFormat ().format (dVal).replace (",", ".");
		noVal = false;
	    } catch (Exception e) {
		e.printStackTrace ();
	    }
	}
	if (noVal)
	    newVal = "";
	return newVal;
    }

    // ========================================
}
