package adecWatt.view.jProp;

import java.awt.Rectangle;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import misc.ScaledImage;

import adecWatt.view.JEditable;

public class JPropImage extends JPImage {
    // ========================================

    public JPropImage (String propName, JEditable jEditable) {
	super (propName, jEditable);
	rightDB = imageDB;
	jImage.addComponentListener (new ComponentAdapter () {
		public void componentResized (ComponentEvent e) {
		    if (scaledImage == null)
			return;
		    Rectangle bounds = jImage.getBounds ();
		    jImage.setIcon (scaledImage.getInsideFit (bounds.width, bounds.height, ScaledImage.defaultAdjust));
		}
	    });
    }

    protected ImageIcon getFitImage () {
	// XXX sauf si déjà "componentResized"
	return scaledImage.getInsideFit (defaultImageWidth, defaultImageHeight, ScaledImage.defaultAdjust);
    }

    protected void displayByType (boolean edit, JPanel jPropList, JTabbedPane jTabbedPane) {
	if (!edit && (hidden || lastValue == null))
	    return;
	setImage ();
	jTabbedPane.add (localizedUserLabel.getCurrentLabel (), jImage);
	jEditable.addLocalizedUserLabel (localizedUserLabel);
	if (!edit)
	    return;
	setPopup ();
    }

    // ========================================
}
