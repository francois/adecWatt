package adecWatt.view.jProp;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.text.JTextComponent;

import misc.Util;
import static misc.Util.GBC;
import static misc.Util.GBCNL;

import adecWatt.view.JEditable;
import adecWatt.view.JProp;
import static adecWatt.model.Prop.PropMix;

public class JPropArticle extends JProp {

    static public final int minRow = 5;

    protected JTextArea editor;
    private boolean unchange;

    // ========================================
    public JPropArticle (String propName, JEditable jEditable) {
	super (propName, jEditable);
    }

    protected void displayByType (boolean edit, JPanel jPropList, JTabbedPane jTabbedPane) {
	if (!edit && lastValue == null)
	    return;
	Util.addComponent (localizedUserLabel.getJLabel (), jPropList, GBC);
	Util.addComponent (new JLabel (" : "), jPropList, GBCNL);
	editor = new JTextArea (lastValue);
	Util.addComponent (Util.getJScrollPane (editor), jPropList, GBCNL);
	editor.setEditable (edit);
	setUnchange (editor, lastValue == PropMix);
	if (edit) {
	    if (editor.getLineCount () < minRow)
		editor.setRows (minRow);
	    setChangeListener (editor);
	}
    }

    public void setUnchange (JTextComponent jTextComponent, boolean unchange) {
	super.setUnchange (jTextComponent, unchange);
	this.unchange = unchange;
    }

    public String getLastValue () {
	String val = editor.getText ();
	if (unchange)
	    val = PropMix;
	return val;
    }

    // ========================================
}
