package adecWatt.view.jProp;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import misc.Util;
import static misc.Util.GBC;
import static misc.Util.GBCLNL;

import adecWatt.view.JEditable;

public class JPropIcon extends JPImage {
    // ========================================
    protected int iconSize = 32;

    public JPropIcon (String propName, JEditable jEditable) {
	super (propName, jEditable);
	rightDB = iconDB;
    }

    public JLabel getMainIcon (boolean edit) {
	if (!edit && lastValue == null)
	    return jImage;
	iconSize = 64;
	setImage ();
	if (edit)
	    setPopup ();
	return jImage;
    }

    protected ImageIcon getFitImage () {
	return scaledImage.getSide (iconSize);
    }

    protected void displayByType (boolean edit, JPanel jPropList, JTabbedPane jTabbedPane) {
	if (!edit && (hidden || lastValue == null))
	    return;
	setImage ();
	Util.addComponent (localizedUserLabel.getJLabel (), jPropList, GBC);
	Util.addComponent (new JLabel (" : "), jPropList, GBC);
	Util.addComponent (jImage, jPropList, GBCLNL);
	if (!edit)
	    return;
	setPopup ();
    }

    // ========================================
}
