package adecWatt.model.unit;

import java.util.Enumeration;

import adecWatt.model.Acc;
import adecWatt.model.AdecWatt;
import adecWatt.model.Prop;
import adecWatt.model.Unit;
import adecWatt.model.UnitNode;

public abstract class NonWorkspace extends Unit<Acc> {
    // ========================================
    static public final String[] workspacesRoot = { Prop.PropBuilding, "lightplot" };

    public NonWorkspace (AdecWatt adecWatt, String id) {
	super (adecWatt, id);
    }

    // ========================================
    public void updateView () {
	for (String workspaceRoot : workspacesRoot)
	    for (Enumeration<?> enumUnitNode = adecWatt.getPermanentDB ().getRootByName (workspaceRoot).getUnitNode ().breadthFirstEnumeration ();
		 enumUnitNode.hasMoreElements ();
		 )
		((Workspace) ((UnitNode<?>) enumUnitNode.nextElement ()).getUnit ()).
		    stateNotifier.broadcastDisplay (BroadcastUpdatePermItem, this);
    }
    // ========================================
}
