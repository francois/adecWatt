package adecWatt.model;

import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class Patch {
    public TreeMap<String, Integer> lineConsumption = new TreeMap<String, Integer> ();
    public TreeMap<String, String> lineCircuit = new TreeMap <String, String> (Circuits.lineComparator);

    public TreeMap<String, Integer> circuitConsumption = new TreeMap<String, Integer> ();
    public TreeMap<String, TreeSet<String>> circuitLines = new TreeMap <String, TreeSet<String>> (Circuits.lineComparator);
    public TreeSet<String> circuitless = new TreeSet<String> ();
    public TreeSet<String> unplug = new TreeSet<String> (Circuits.lineComparator);
    public TreeSet<String> shortcut = new TreeSet<String> (Circuits.lineComparator);

    private void addConsumption (TreeMap<String, Integer> map, String name, int value) {
	Integer oldValue = map.get (name);
	map.put (name, oldValue == null ? value : oldValue+value);
    }

    public Set<String> getLineKeys () {
	TreeSet<String> result = new TreeSet<String> (Circuits.lineComparator);
	result.addAll (lineCircuit.keySet ());
	return result;
    }

    public Set<String> getCircuitKeys () {
	TreeSet<String> result = new TreeSet<String> (Circuits.lineComparator);
	result.addAll (circuitLines.keySet ());
	return result;
    }

    public Patch (Circuits circuits) {
	for (Comp lastComp : circuits.keySet ()) {
	    Circuits.Circuit circuit = circuits.get (lastComp);
	    String lineName = lastComp.getLine ();
	    if (lineName == null)
		continue;
	    TreeSet<String> circuitNames = circuit.names;
	    int consumption = circuit.consumption;
	    switch (circuitNames.size ()) {
	    case 0:
		continue;
	    case 1:
		String circuitName = circuitNames.first ();
		addConsumption (lineConsumption, lineName, consumption);
		lineCircuit.put (lineName, circuitName);
		addConsumption (circuitConsumption, circuitName, consumption);
		TreeSet<String> otherLines = circuitLines.get (circuitName);
		if (otherLines == null)
		    circuitLines.put (circuitName, otherLines = new TreeSet<String> (Circuits.lineComparator));
		otherLines.add (lineName);
		break;
	    default:
		lineCircuit.put (lineName, Circuits.SHORTCUT);
		for (String circuitName2 : circuitNames)
		    shortcut.add (circuitName2);
	    }
	}
	for (Comp beginComp : circuits.getPlugedComps ())
	    accLoop :
	    for (Acc beginAcc : beginComp.allPlugs) {
		String circuitName = beginAcc.getCircuit (beginComp);
		String compName = beginComp.getName ();
		if (circuits.inLoop (beginAcc)) {
		    if (circuitName != null)
			unplug.add (circuitName);
		    else
			circuitless.add (compName);
		    continue;
		}
		for (Comp lastComp : circuits.keySet ()) {
		    Circuits.Circuit circuit = circuits.get (lastComp);
		    if (!circuit.nodes.contains (beginAcc))
			continue;
		    if (lastComp.getLine () == null)
			if (circuitName != null)
			    unplug.add (circuitName);
			else
			    circuitless.add (compName);
		    continue accLoop;
		}
		if (circuitName != null)
		    unplug.add (circuitName);
		else
		    circuitless.add (compName);
	    }
    }
}
