package adecWatt.model.xml;

import org.w3c.dom.Node;

import adecWatt.model.Comp;
import adecWatt.model.Permanent;

public class XmlComp extends XmlPermanent<Permanent.CompTypeEnum, Permanent.CompAttrEnum> {

    // ========================================
    public XmlComp (Node node) {
	parseNode (node, Comp.CompTypeEnum.class, Comp.CompAttrEnum.class);
    }

    // ========================================
}
