package adecWatt.model.xml;

import org.w3c.dom.Node;

import adecWatt.model.Segm;
import adecWatt.model.Permanent;

public class XmlSegm extends XmlPermanent<Permanent.SegmTypeEnum, Permanent.SegmAttrEnum> {

    // ========================================
    public XmlSegm (Node node) {
	parseNode (node, Segm.SegmTypeEnum.class, Segm.SegmAttrEnum.class);
    }

    // ========================================
}
