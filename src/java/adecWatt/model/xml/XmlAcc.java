package adecWatt.model.xml;

import org.w3c.dom.Node;

import adecWatt.model.Acc;
import adecWatt.model.Permanent;

public class XmlAcc extends XmlPermanent<Permanent.AccTypeEnum, Permanent.AccAttrEnum> {

    // ========================================
    public XmlAcc (Node node) {
	parseNode (node, Acc.AccTypeEnum.class, Acc.AccAttrEnum.class);
    }

    // ========================================
}
