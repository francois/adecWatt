package adecWatt.model.xml;

import org.w3c.dom.Node;

import adecWatt.model.Permanent;
import adecWatt.model.Prop;

/**
   text|icon|image|enum|number|geo|square|cube|article
*/
public class XmlProp extends XmlPermanent<Permanent.PropTypeEnum, Permanent.PropAttrEnum> {

    // ========================================
    public XmlProp (Node node) {
	parseNode (node, Prop.PropTypeEnum.class, Prop.PropAttrEnum.class);
    }

    // ========================================
}
