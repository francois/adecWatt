package adecWatt.control;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Frame;
import java.awt.Image;
import java.text.MessageFormat;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import misc.Bundle;
import misc.Config;
import misc.Controller;
import misc.HelpManager;
import misc.HtmlDialog;
import misc.MultiToolBarBorderLayout;
import misc.ProxyManager;
import misc.RemoteUpdate;
import misc.RemoteUpdateManager;
import misc.StoryManager;
import misc.ToolBarManager;
import misc.Util;

import adecWatt.model.AdecWatt;
import adecWatt.model.Unit;
import adecWatt.model.User;
import adecWatt.view.JAdecWatt;
import adecWatt.view.JAdecWattDialog;
import adecWatt.view.JAdecWattMenuBar;
import adecWatt.view.JAdecWattSearchToolBar;
import adecWatt.view.JAdecWattToolBar;
import adecWatt.view.JWorkspaceSlidersToolBar;

public class AdecWattController extends Controller<AdecWatt> {

    // ========================================
    AdecWatt adecWatt;
    RemoteUpdate remoteUpdate;

    JAdecWatt jAdecWatt;

    JAdecWattMenuBar jAdecWattMenuBar;
    JAdecWattToolBar jAdecWattToolBar;
    JAdecWattDialog jAdecWattDialog;
    JAdecWattSearchToolBar jAdecWattSearchToolBar;
    JWorkspaceSlidersToolBar jWorkspaceSlidersToolBar;

    HtmlDialog manualDialog;

    AdecWattManager adecWattManager;
    HelpManager helpManager;
    ToolBarManager toolBarManager;
    StoryManager storyManager;
    ProxyManager proxyManager;
    RemoteUpdateManager remoteUpdateManager;

    // ========================================
    public AdecWattController (AdecWatt adecWatt) {
	super (adecWatt);
    }

    public void restart () {
	adecWattManager.actionRestart ();
    }

    // ========================================
    protected void createModel (AdecWatt adecWatt) {
	this.adecWatt = adecWatt;
	adecWatt.addUpdateObserver (this, AdecWatt.BroadcastTitle);
	adecWatt.getUser ().addUpdateObserver (this, User.BroadcastUserChangeProfil);
	Bundle.addBundleObserver (this);
	remoteUpdate =
	    new RemoteUpdate (AdecWattManager.protocol, AdecWattManager.remoteServerName, AdecWattManager.stableVersion, AdecWattManager.urlModel,
			      new String [] {"soft"},
			      new String [] {"data", "data", "server"},
			      new String [] {"help-fr_FR", "data", "texts", "help-fr_FR"},
			      new String [] {"help-images", "data", "texts", "help-images"},
			      new String [] {"help-en_US", "data", "texts", "help-en_US"},
			      new String [] {"help-br_FR_breton", "data", "texts", "help-br_FR_breton"},
			      new String [] {"help-br_FR_gallo", "data", "texts", "help-br_FR_gallo"},
			      new String [] {"help-es_ES", "data", "texts", "help-es_ES"}
			      //new String [] {"test", "data", "test", "server"}
			      );
    }

    // ========================================
    protected Component createGUI () {
	jAdecWatt = new JAdecWatt (adecWatt);
	jAdecWattDialog = new JAdecWattDialog (this);
	jWorkspaceSlidersToolBar = new JWorkspaceSlidersToolBar (jAdecWatt);
	jAdecWattSearchToolBar = new JAdecWattSearchToolBar (jAdecWatt);
	manualDialog = new HtmlDialog (jFrame, "Manual", "data/texts/help-"+Bundle.getLocale ()+"/index.html");

	Frame frame = new Frame ();
	frame.setIconImage (getIcon ());
	JPanel contentPane = new JPanel (new MultiToolBarBorderLayout ());
	toolBarManager = new ToolBarManager (getIcon (), contentPane);

	storyManager = new StoryManager ();
	remoteUpdateManager = new RemoteUpdateManager (this, remoteUpdate,
						       new Runnable () {
							   public void run () {
							       restart ();
							   }
						       });
	remoteUpdateManager.check ();
	adecWattManager =
	    new AdecWattManager (adecWatt, storyManager, remoteUpdateManager,
				 jAdecWatt, jWorkspaceSlidersToolBar, jAdecWattDialog, manualDialog);
	helpManager = new HelpManager (this, "AdecWatt");
	proxyManager = new ProxyManager (this);
	jAdecWattToolBar = new JAdecWattToolBar (this,
						   adecWattManager, storyManager, proxyManager, remoteUpdateManager, helpManager, toolBarManager,
						   jAdecWattSearchToolBar, jWorkspaceSlidersToolBar);
	contentPane.add (jAdecWatt, BorderLayout.CENTER);

	return contentPane;
    }

    protected void newJFrame () {
	manualDialog.setJFrame (jFrame);
	// XXX pas RemoteUpdate ! et les outres ?
    }

    public void updateBundle () {
	super.updateBundle ();
	SwingUtilities.invokeLater (new Runnable () {
		public void run () {
		    manualDialog.changeFileName ("data/texts/help-"+Bundle.getLocale ()+"/index.html");
		}
	    });
    }

    // ========================================
    public String getTitle () {
	Unit<?> currentWorkspace = jAdecWatt.getCurrentWorkspace ();
	Unit<?> sharedUnitStory = adecWattManager.getSharedUnitStory ();
	return MessageFormat.format (Bundle.getTitle ("AdecWatt"),
				     currentWorkspace == null ? Bundle.getAction ("Empty") : currentWorkspace.toString (),
				     sharedUnitStory == null ? "?" : sharedUnitStory.toString ());
    }

    public Image getIcon () {
	try {
	    return Util.loadImage (Config.getString ("AdecWattIcon", "data/images/adecWatt.png"));
	} catch (Exception e) {
	    return null;
	}
    }

    public void updateTitle () {
	updateBundle ();
    }
    public void updateUserChangeProfil () {
	User user = adecWatt.getUser ();
	if (jAdecWattMenuBar != null)
	    jAdecWattMenuBar.changeProfil (user);
	if (jAdecWattToolBar != null)
	    jAdecWattToolBar.changeProfil (user);
    }

    // ========================================
    protected JMenuBar createMenuBar () {
	return jAdecWattMenuBar = new JAdecWattMenuBar (this, adecWattManager, storyManager, proxyManager, remoteUpdateManager, helpManager, toolBarManager);
    }

    // ========================================
    protected boolean tryClosingWindows () {
	Config.save ("AdecWatt");
	if (adecWatt.getModified ())
	    switch (JOptionPane.showConfirmDialog (jFrame, Bundle.getMessage ("QuitJAdecWatt"),
						   Bundle.getTitle ("AdecWattStillRunning"),
						   JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE)) {
	    case JOptionPane.YES_OPTION:
		return true;
	    case JOptionPane.NO_OPTION:
	    case JOptionPane.CANCEL_OPTION:
	    case JOptionPane.CLOSED_OPTION:
		return false;
	    }
	return true;
    }

    // ========================================
}
