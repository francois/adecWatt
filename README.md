AdecWatt
=======

## Install

git clone git@framagit.org:adecwatt/adecWatt.git

ant -f ant/build.xml

git config user.email "You@example.com"

git config user.name "Your Name"



## data server

[adecwatt.parlenet.org](http://adecwatt.parlenet.org/)

## Organization

[ADEC 56](http://adec56.org/spip/index.php)

## Remote commands

  * getRoles 
  * zipList path
  * zipGets path
  * zipPuts zip
  * zipRemove path

## Test

https://adecwatt.parlenet.org/lib/plugins/adecwatt/adecWattBD.php?version=v3&action=getRoles
https://adecwatt.parlenet.org/lib/plugins/adecwatt/adecWattBD.php?version=v3&action=zipList&name=data

